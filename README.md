# Project Name
> Spring Boot Rss Feed Reader

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Status](#status)
* [Contact](#contact)

## General info
Spring Boot application that polls new feeds every 5 minutes from http://feeds.nos.nl/nosjournaal?format=xml

## Technologies
* Spring Boot
* Spring Integration
* GraphQL 

## Setup
Clone or download the source from bitbucket, import the source code as a Maven project and run it as a spring boot application.
The application will be started automatically and every 5 minutes will poll new feeds from http://feeds.nos.nl/nosjournaal?format=xml
Install Altair GraphQL Client. Sent HTTP POST request to: http://localhost:8080/graphql
With the following query:
{
  feeds {
  title 
  description 
  publishedDate
  imageLink
}
}
And the query will return all rss feeds from the database.

## Status
Project is:  _finished

## Contact
Created by [@vineta](https://www.linkedin.com/in/vineta-andreevska-stankova-a1633a46) - feel free to contact me!