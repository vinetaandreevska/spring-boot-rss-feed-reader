package com.example.rss.feed.springbootrssfeedreader.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.rss.feed.springbootrssfeedreader.model.FeedItem;

public interface FeedItemRepository extends JpaRepository<FeedItem, Long> {

}
