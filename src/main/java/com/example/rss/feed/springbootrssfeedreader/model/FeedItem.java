package com.example.rss.feed.springbootrssfeedreader.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Data;

@Entity
@Data
public class FeedItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private Long id;
	@GraphQLQuery(name = "title", description = "A feed's title")
	private String title;
	@Lob
	@GraphQLQuery(name = "description", description = "A feed's description")
	private String description;
	@GraphQLQuery(name = "publishedDate", description = "A feed's publishedDate")
	private Date publishedDate;
	@GraphQLQuery(name = "imageLink", description = "A feed's imageLink")
	private String imageLink;

}
