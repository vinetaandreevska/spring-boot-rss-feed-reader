package com.example.rss.feed.springbootrssfeedreader.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.rss.feed.springbootrssfeedreader.model.FeedItem;
import com.example.rss.feed.springbootrssfeedreader.services.FeedItemService;

@RestController
@RequestMapping("/feeds")
public class FeedItemController {

	private final FeedItemService feedItemService;

	@Autowired
	public FeedItemController(FeedItemService feedItemService) {
		this.feedItemService = feedItemService;
	}

	@GetMapping
	public List<FeedItem> getFeed() {
		return feedItemService.syncNewFeeds();
	}

	@PostMapping
	public FeedItem save(@Valid @RequestBody FeedItem feedItem) {
		return feedItemService.saveFeedItem(feedItem);
	}
}
