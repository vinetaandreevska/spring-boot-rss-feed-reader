package com.example.rss.feed.springbootrssfeedreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpringBootRssFeedReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRssFeedReaderApplication.class, args);
	}
}
