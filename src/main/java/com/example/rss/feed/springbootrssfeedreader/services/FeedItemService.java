package com.example.rss.feed.springbootrssfeedreader.services;

import java.util.List;

import javax.validation.Valid;

import com.example.rss.feed.springbootrssfeedreader.model.FeedItem;

public interface FeedItemService {

	List<FeedItem> syncNewFeeds();

	FeedItem saveFeedItem(@Valid FeedItem feedItem);

	List<FeedItem> getAllFeeds();
}
