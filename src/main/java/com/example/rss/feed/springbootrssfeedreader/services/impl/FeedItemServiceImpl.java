package com.example.rss.feed.springbootrssfeedreader.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.PollableChannel;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.rss.feed.springbootrssfeedreader.model.FeedItem;
import com.example.rss.feed.springbootrssfeedreader.repositories.FeedItemRepository;
import com.example.rss.feed.springbootrssfeedreader.services.FeedItemService;
import com.rometools.rome.feed.synd.SyndEntry;

import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;

@Service
@GraphQLApi
public class FeedItemServiceImpl implements FeedItemService {

	private final FeedItemRepository feedItemRepository;

	@Autowired
	public FeedItemServiceImpl(FeedItemRepository feedItemRepository) {
		this.feedItemRepository = feedItemRepository;
	}

	@Override
	@Scheduled(cron = "0 0/5 * * * ?")
	public List<FeedItem> syncNewFeeds() {
		List<FeedItem> feedItems = new ArrayList<FeedItem>();
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"skipDuplicateMessagesFeedContext.xml");
		try {
			PollableChannel feedChannel = context.getBean("articleChannel", PollableChannel.class);
			for (int i = 0; i < 10; i++) {
				Message<SyndEntry> message = (Message<SyndEntry>) feedChannel.receive(10000);
				if (message != null) {
					SyndEntry entry = message.getPayload();
					FeedItem feedItem = new FeedItem();
					feedItem.setTitle(entry.getTitle());
					feedItem.setDescription(entry.getDescription().getValue());
					feedItem.setPublishedDate(entry.getPublishedDate());
					feedItem.setImageLink(entry.getEnclosures().get(0).getUrl());
					feedItems.add(feedItem);
					saveFeedItem(feedItem);
				} else {
					break;
				}
			}
		} finally {
			context.close();
		}
		return feedItems;
	}

	@Override
	public FeedItem saveFeedItem(@Valid FeedItem feedItem) {
		return feedItemRepository.save(feedItem);
	}
	
	@Override
	@GraphQLQuery(name = "feeds")
    public List<FeedItem> getAllFeeds() {
        return feedItemRepository.findAll();
    }

}
